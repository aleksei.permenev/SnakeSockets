from SnakeClass.tools import *


class Snake:
    def __init__(self, par: Tools):
        self.par = par
        self.VELS = [Vector2d(0, -1, self.par), Vector2d(1, 0, self.par),
                     Vector2d(0, 1, self.par), Vector2d(-1, 0, self.par)]
        self.head = self.par.get_random_vector2d()
        self.tail = [
            self.head,
            self.head - Vector2d(1, 0, self.par),
            self.head - Vector2d(2, 0, self.par)
        ]
        self.is_eat = False
        self.is_wall = False
        self.direction = 1
        self.next_direction = 1
        self.eat = self.par.get_random_vector2d()
        while self.eat in self.tail:
            self.eat = self.par.get_random_vector2d()

    def move(self):
        self.is_wall = False
        self.is_eat = False
        self.direction = self.next_direction
        self.head += self.VELS[self.direction]
        self.eat += Vector2d(0, 0, self.par)
        if self.head in self.tail:
            self.is_wall = True
        elif self.head == self.eat:
            self.is_eat = True
        is_add = False
        if self.is_wall:
            return False
        elif self.is_eat:
            is_add = True
            self.eat = self.par.get_random_vector2d()
            while self.eat in self.tail:
                self.eat = self.par.get_random_vector2d()
            self.tail.append(self.tail[-1])
        if is_add:
            for i in range(len(self.tail) - 2, 0, -1):
                self.tail[i] = self.tail[i - 1]
            self.tail[0] = self.head
        else:
            for i in range(len(self.tail) - 1, 0, -1):
                self.tail[i] = self.tail[i - 1]
            self.tail[0] = self.head
        return True
